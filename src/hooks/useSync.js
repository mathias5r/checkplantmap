/* eslint-disable react-hooks/exhaustive-deps */
import {useEffect} from 'react';
import syncService from '../services/sync';
import {useSelector, useDispatch} from 'react-redux';

export default (onFinish) => {
  const locations = useSelector((state) => state.User.locations);
  const dispatch = useDispatch();
  useEffect(() => {
    if (locations?.length > 0) {
      syncService.syncLocations({locations, dispatch, onFinish});
      return;
    }
    onFinish();
  }, []);
};
