import {useEffect, useState} from 'react';
import GeolocationService from '../services/geolocation';
import {TEXTS} from '../constants';
import logger from '../services/logger';

const onSucess = (setFirstLocation) => (info) => {
  const {latitude, longitude} = info?.coords || {};
  if (latitude && longitude) {
    setFirstLocation({latitude, longitude});
  }
};

export default () => {
  const [firstLocation, setFirstLocation] = useState(null);
  useEffect(() => {
    GeolocationService.getPosition({
      onSuccess: (info) => onSucess(setFirstLocation)(info),
      onError: () => logger.error(TEXTS.LOCATION_ERROR),
    });
  }, []);
  return firstLocation;
};
