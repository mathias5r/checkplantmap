import React from 'react';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {useSelector} from 'react-redux';
import {StyleSheet, ActivityIndicator} from 'react-native';
import styled from 'styled-components';
import CenterView from '../library/CenterView';
import Pin from '../library/Pin';
import {TEXTS} from '../constants';

const LoadingView = styled(CenterView)`
  flex: 1;
`;

const Text = styled.Text`
  font-size: 16px;
  padding: 20px;
`;

const MapComponent = ({firstLocation}) => {
  const locations = useSelector((state) => state.User.locations);
  if (!firstLocation) {
    return (
      <LoadingView>
        <Text>{TEXTS.GETTING_LOCATION}</Text>
        <ActivityIndicator size="large" color="green" />
      </LoadingView>
    );
  }
  return (
    <MapView
      provider={PROVIDER_GOOGLE}
      style={{...StyleSheet.absoluteFillObject}}
      region={{
        ...firstLocation,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      }}>
      {locations.map((location, index) => (
        <Marker
          key={`${index.toString()}-${location.sync ? 'active' : 'inactive'}`}
          coordinate={location.coordinate}
          title={location.title}
          description={location.description}>
          <Pin color={location.sync ? 'gray' : 'green'} />
        </Marker>
      ))}
    </MapView>
  );
};

export default MapComponent;
