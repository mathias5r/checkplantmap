import React from 'react';
import {Modal, ActivityIndicator} from 'react-native';
import styled from 'styled-components';
import {TEXTS} from '../constants';
import CenterView from '../library/CenterView';
import useSync from '../hooks/useSync';

const Center = styled(CenterView)`
  flex: 1;
`;

const Body = styled(CenterView)`
  height: 20%;
  width: 80%;
  background-color: white;
  border-radius: 25px;
`;

const Text = styled.Text`
  font-size: 16px;
  padding: 20px;
`;

const SyncModal = ({visible, setVisible}) => {
  useSync(() => setVisible(false));
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => setVisible(false)}>
      <Center>
        <Body>
          <Text>{TEXTS.SYNCHRONIZING}</Text>
          <ActivityIndicator />
        </Body>
      </Center>
    </Modal>
  );
};

export default SyncModal;
