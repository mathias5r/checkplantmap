import React, {useState} from 'react';
import FormModal from './FormModal';
import styled from 'styled-components';
import Button from '../library/Button';
import {TEXTS} from '../constants';
import MapComponent from './MapComponent';
import SyncModal from './SyncModal';
import ErrorModal from './ErrorModal';
import useLocation from '../hooks/useLocation';

const Container = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

const ButtonsView = styled.View`
  height: 10%;
  flex-direction: row;
`;

const HomeComponent = () => {
  const [showFormModal, setFormModalStatus] = useState(false);
  const [showSyncModal, setSyncModalStatus] = useState(false);
  const [error, setError] = useState('');
  const firstLocation = useLocation();
  return (
    <Container>
      <MapComponent {...{firstLocation}} />
      <ButtonsView>
        <Button
          onPress={() => setFormModalStatus(true)}
          text={TEXTS.NEW_MARKER}
          disabled={!firstLocation}
        />
        <Button
          onPress={() => setSyncModalStatus(true)}
          text={TEXTS.SYNC}
          isRight
          disabled={!firstLocation}
        />
      </ButtonsView>
      {showFormModal && (
        <FormModal
          visible
          setVisible={setFormModalStatus}
          setError={setError}
        />
      )}
      {showSyncModal && (
        <SyncModal
          visiable
          setVisible={setSyncModalStatus}
          setError={setError}
        />
      )}
      {!!error && (
        <ErrorModal visiable error={error} setVisible={() => setError('')} />
      )}
    </Container>
  );
};

export default HomeComponent;
