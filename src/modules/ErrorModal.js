import React from 'react';
import {Modal} from 'react-native';
import styled from 'styled-components';
import {TEXTS} from '../constants';
import CenterView from '../library/CenterView';

const Center = styled(CenterView)`
  flex: 1;
`;

const Body = styled(CenterView)`
  height: 20%;
  width: 80%;
  background-color: white;
  border-radius: 25px;
`;

const CloseButton = styled.TouchableOpacity``;

const Text = styled.Text`
  font-size: 16px;
  padding: 20px;
`;

const SyncModal = ({error, visible, setVisible}) => {
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => setVisible(false)}>
      <Center>
        <Body>
          <Text>{error}</Text>
          <CloseButton onPress={() => setVisible(false)}>
            <Text>{TEXTS.CLOSE}</Text>
          </CloseButton>
        </Body>
      </Center>
    </Modal>
  );
};

export default SyncModal;
