import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Modal, TextInput, Keyboard, Platform} from 'react-native';
import GeolocationService from '../services/geolocation';
import styled from 'styled-components';
import Button from '../library/Button';
import {TEXTS} from '../constants';
import {handleSuccess} from '../services/local';

const Header = styled.TouchableOpacity`
  height: 20%;
  width: 100%;
  background-color: transparent;
`;

const Body = styled.TouchableOpacity`
  height: 70%;
  width: 100%;
  background-color: white;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  padding-top: 25px;
`;

const CloseButton = styled.TouchableOpacity`
  padding: 0 30px;
  align-self: flex-end;
`;

const Text = styled.Text`
  font-size: 16px;
`;

const Title = styled(Text)`
  padding: 0 20px;
  text-align: center;
`;

const InputView = styled.View`
  border-bottom-color: black;
  border-bottom-width: 1px;
  margin: 0 30px;
`;

const Input = styled.TextInput`
  padding-bottom: ${Platform.OS === 'ios' ? '10px' : '3px'};
`;

const FormModal = ({visible, setVisible, setError}) => {
  const locations = useSelector((state) => state.User.locations);
  const dispatch = useDispatch();
  const [value, onChangeText] = useState('');
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => setVisible(false)}>
      <Header onPress={() => setVisible(false)} />
      <Body activeOpacity={1} onPress={() => Keyboard.dismiss()}>
        <CloseButton onPress={() => setVisible(false)}>
          <Text>{TEXTS.CLOSE}</Text>
        </CloseButton>
        <InputView>
          <Title>{TEXTS.INSTRUCTION}</Title>
          <Input
            onChangeText={(text) => onChangeText(text)}
            value={value}
            placeholder={TEXTS.DESCRIPTION}
            multiline
          />
        </InputView>
      </Body>
      <Button
        onPress={() => {
          setVisible(false);
          GeolocationService.getPosition({
            onSuccess: (info) =>
              handleSuccess({dispatch, locations, description: value})(info),
            onError: (err) => setError(err),
          });
        }}
        text={TEXTS.SAVE}
      />
    </Modal>
  );
};

export default FormModal;
