import React from 'react';
import HomeComponent from './modules/HomeComponent';
import {Provider} from 'react-redux';
import {store, persistor} from './redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {ActivityIndicator} from 'react-native';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={<ActivityIndicator />} persistor={persistor}>
        <HomeComponent />
      </PersistGate>
    </Provider>
  );
};

export default App;
