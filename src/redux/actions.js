export const SET_LOCATIONS = 'SET_LOCATIONS';

export const setLocations = (value) => ({
  type: SET_LOCATIONS,
  locations: value,
});
