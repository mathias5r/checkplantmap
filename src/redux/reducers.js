import {SET_LOCATIONS} from './actions';

const initialState = {
  locations: [],
};

export const User = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOCATIONS:
      return {
        ...state,
        locations: action.locations,
      };
    default:
      return state;
  }
};
