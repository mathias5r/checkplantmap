import React from 'react';
import styled from 'styled-components';

const Container = styled.TouchableOpacity`
  flex: 1;
  background-color: gray;
  justify-content: center;
  align-items: center;
  border-left-width: ${({isRight}) => (isRight ? '1px' : 0)};
  border-left-color: white;
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
`;

const Text = styled.Text`
  color: white;
  font-size: 16px;
`;

const Button = ({text, onPress, isRight = false, disabled}) => (
  <Container disabled={disabled} activeOpacity={1} {...{onPress, isRight}}>
    <Text>{text}</Text>
  </Container>
);

export default Button;
