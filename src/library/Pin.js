import styled from 'styled-components';

const Pin = styled.View`
  width: 0;
  height: 0;
  background-color: transparent;
  border-style: solid;
  border-left-width: 15px;
  border-right-width: 15px;
  border-bottom-width: 30px;
  border-left-color: transparent;
  border-right-color: transparent;
  border-bottom-color: ${({color}) => color};
  transform: rotate(180deg);
`;

export default Pin;
