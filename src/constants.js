export const LOCATION_STATUS = {
  UNAVAILABLE: 'unavailable',
  DENIED: 'denied',
  GRANTED: 'granted',
  BLOCKED: 'blocked',
};

export const USER_EMAIL = 'mathiassilva4@gmail.com';

export const ENDPOINTS = {
  CHECK_PLANT_SERVER: 'https://hooks.zapier.com/hooks/catch/472009/09rj5z/',
};

export const TEXTS = {
  CLOSE: 'Fechar',
  SAVE: 'Salvar',
  DESCRIPTION: 'Descrição',
  NEW_MARKER: 'Novo Ponto',
  SYNC: 'Sincronizar',
  INSTRUCTION: 'Digite abaixo a descrição para sua nova localização:',
  SYNCHRONIZING: 'Sincronização em andamento...',
  GETTING_LOCATION: 'Obtendo geolocalização...',
  LOCATION_ERROR: 'Erro ao tentar obter posição',
  SYNC_ERROR: 'Erro ao sincronizar localização',
};

export const TIMEOUTS = {
  TWENTY_SECONDS: 20000,
  TEN_SECONDS: 10000,
  FIVE_SECONDS: 5000,
};
