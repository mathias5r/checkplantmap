import {setLocations} from '../redux/actions';

export const handleSuccess = ({dispatch, locations, description}) => (info) => {
  const {longitude, latitude} = info.coords;
  dispatch(
    setLocations([
      ...locations,
      {
        coordinate: {longitude, latitude},
        title: new Date().toISOString(),
        description,
      },
    ]),
  );
};
