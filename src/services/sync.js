import axios from 'axios';
import {ENDPOINTS, USER_EMAIL, TEXTS} from '../constants';
import {setLocations} from '../redux/actions';
import logger from './logger';

const syncLocations = async ({locations, dispatch, onFinish}) => {
  const promisses = locations.map((location) => {
    if (location?.sync) {
      return location;
    }
    const {longitude, latitude} = location.coordinate;
    return axios
      .post(
        `${ENDPOINTS.CHECK_PLANT_SERVER}?email_key=${USER_EMAIL}`,
        {
          longitude: longitude,
          latitude: latitude,
          annotation: location.description,
          datetime: location.title,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      )
      .then(() => ({...location, sync: true}))
      .catch((error) => {
        logger.error(`${TEXTS.SYNC_ERROR}`, JSON.stringify(location), error);
        return location;
      });
  });
  const result = await Promise.all(promisses);
  onFinish();
  dispatch(setLocations(result));
};

export default {
  syncLocations,
};
