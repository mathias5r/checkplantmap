import Geolocation from '@react-native-community/geolocation';
import {check, request, PERMISSIONS} from 'react-native-permissions';
import {Platform} from 'react-native';
import {LOCATION_STATUS, TIMEOUTS, TEXTS} from '../constants';

const PERMISSION_NAME =
  Platform.OS === 'ios' ? 'LOCATION_WHEN_IN_USE' : 'ACCESS_FINE_LOCATION';

const permission = PERMISSIONS[Platform.OS.toUpperCase()][PERMISSION_NAME];

const requestPermission = async () => {
  const status = await check(permission);
  if (status === LOCATION_STATUS.GRANTED) {
    return {error: false, value: ''};
  }
  if (status === LOCATION_STATUS.DENIED) {
    const result = await request(permission);
    if (result === LOCATION_STATUS.GRANTED) {
      return {error: false, value: ''};
    }
    return {error: true, value: result};
  }
  return {error: true, value: status};
};

const highAccuracyConfigurations = {
  enableHighAccuracy: true,
  maximumAge: TIMEOUTS.TWENTY_SECONDS,
  timeout: TIMEOUTS.FIVE_SECONDS,
};

const lowAccuracyConfigurations = {
  enableHighAccuracy: false,
  maximumAge: TIMEOUTS.TWENTY_SECONDS,
  timeout: TIMEOUTS.TEN_SECONDS,
};

const getPosition = async ({onSuccess, onError}) => {
  const {error, value} = await requestPermission();
  if (error) {
    onError(`${TEXTS.LOCATION_ERROR}: ${value}`);
    return;
  }
  const onErrorTryWithLowAccuracy = () =>
    Geolocation.getCurrentPosition(
      onSuccess,
      onError,
      lowAccuracyConfigurations,
    );

  Geolocation.getCurrentPosition(
    onSuccess,
    onErrorTryWithLowAccuracy,
    highAccuracyConfigurations,
  );
};

export default {
  getPosition,
};
