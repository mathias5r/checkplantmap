# Checkplant Map

## Installation

```bash
yarn install

# for ios
cd ios && pod install
```

## Usage

```bash
react-native run-ios
react-native run-android
```

## Used Libraries

* @react-native-community/async-storage
* @react-native-community/geolocation
* axios
* react-native-maps
* react-native-permissions
* react-redux
* redux
* redux-persist
* styled-components
  